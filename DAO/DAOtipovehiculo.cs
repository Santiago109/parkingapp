using System.Collections.Generic;
using System.Linq;
using ParkingApp.Models;

namespace ParkingApp.DAO
{
    public class DAOtipovehiculo{        
        private  ParkingAppContext _db;
        public DAOtipovehiculo( ParkingAppContext context){
                _db =  context;
        }
        public IEnumerable<tipo_vehiculo> getTipoVehiculo(){
            if(_db != null){
                        return _db.tipo_vehiculo.ToList();
            }
            return null;
        }
        public tipo_vehiculo getTipoVehiculoById(int id){
            return _db.tipo_vehiculo.Find(id);
        }
        public void insertTipoVehiculo(tipo_vehiculo tipovehiculo){
            _db.tipo_vehiculo.Add(tipovehiculo);
            _db.SaveChanges();
        }
       public void updateTipoVehiculo(tipo_vehiculo tipovehiculo){
            _db.tipo_vehiculo.Update(tipovehiculo);
            _db.SaveChanges();
       }
       public void deleteTipoVehiculo(int id){
            _db.tipo_vehiculo.Remove( _db.tipo_vehiculo.Find(id));
             _db.SaveChanges();
       }
    }

}