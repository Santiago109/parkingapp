using System;
using System.Collections.Generic;
using System.Linq;
using ParkingApp.Models;

namespace ParkingApp.DAO
{
    public class DAOEmpleados{
        
        private  ParkingAppContext _db;

        public DAOEmpleados( ParkingAppContext context){
            _db =  context;
        }
      
        
       public IEnumerable<empleados> getEmpleados(){

           if(_db != null){
                return _db.empleados.ToList();
           }

           return null;
           
       }
        public empleados getEmpleadoById(int id){
            
            return _db.empleados.Find(id);
       }
       public void insertEmpleado(empleados empleado){
            _db.empleados.Add(empleado);
            _db.SaveChanges();
       }

       public void updateEmpleado(  empleados empleado){
            _db.empleados.Update( empleado);
            _db.SaveChanges();
       }

       public void deleteEmpleado(int id){
            _db.empleados.Remove( _db.empleados.Find(id));
             _db.SaveChanges();
       }

       public IEnumerable<empleados> empleadoLogin (string identificacion, string contrasena ){

             return _db.empleados.Where(
                  e =>  e.identificacion == identificacion 
                      && e.password  == contrasena
             ).ToList();
       }


        public empleados registraEmpleado(empleados empleado){
            _db.empleados.Add(empleado);
            _db.SaveChanges();

             return empleado;

       }



       public IEnumerable<object> getReporte(Int32 year, Int32 month){
            
        


           if(month == 0 && year != 0){

                var resultado = from s in _db.registro_ingresos
                join sa in _db.vehiculos on s.id_vehiculo equals sa.id_vehiculo
                join em in _db.empleados on sa.id_empleado equals em.id_empleado
                where (s.fecha.Year == year)
                group s  by em.nombre into g
                select new {
                               empleado= g.Key,
                               cantidad = g.Count(),
                               
                           }; 
            return resultado;

           }else{

               if(year ==0){

var resultado =  from s in _db.registro_ingresos
                join sa in _db.vehiculos on s.id_vehiculo equals sa.id_vehiculo
                join em in _db.empleados on sa.id_empleado equals em.id_empleado
               
                group s  by em.nombre into g
                select new {
                               empleado= g.Key,
                               cantidad = g.Count(),
                               
                           }; 

            return resultado;

               }else{
                   var resultado =  from s in _db.registro_ingresos
                join sa in _db.vehiculos on s.id_vehiculo equals sa.id_vehiculo
                join em in _db.empleados on sa.id_empleado equals em.id_empleado
                where   (s.fecha.Year.ToString() == year.ToString() && s.fecha.Month == month)
                group s  by em.nombre into g
                select new {
                               empleado= g.Key,
                               cantidad = g.Count(),
                               
                           }; 

            return resultado;

               }
      
           }
        }

    }
}