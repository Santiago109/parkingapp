using System.Collections.Generic;
using System.Linq;
using ParkingApp.Models;

namespace ParkingApp.DAO
{

    public class DAOVehiculos{
        

        private  ParkingAppContext _db;

        public DAOVehiculos( ParkingAppContext context){
            _db =  context;
        }
      
        
       public IEnumerable<vehiculos> getVehiculos(){

           if(_db != null){
                return _db.vehiculos.ToList();
           }
           return null;           
       }
        public vehiculos getVehiculoById(int id){
            
            return _db.vehiculos.Find(id);
       }


        public IEnumerable<object> getVehiculoByEmpleado(int _id){
            
            var vehiculo = _db.vehiculos.Where(
                v => v.id_empleado == _id
            ).Select(x=> new {tipoVehiculo=x.Tipo_Vehiculo.tipo, vehiculo= x } ).ToList();

            return vehiculo;
       }

       public void insertVehiculo(vehiculos vehiculo){
            _db.vehiculos.Add(vehiculo);
            _db.SaveChanges();
       }

       public void updateVehiculo(vehiculos vehiculo){
            _db.vehiculos.Update(vehiculo);
            _db.SaveChanges();
       }

       public void deleteVehiculo(int id){

          
            _db.vehiculos.Remove(_db.vehiculos.Find(id) );
            _db.SaveChanges();
       }


        public vehiculos registraVehiculo(vehiculos vehiculo){
            _db.vehiculos.Add(vehiculo);
            _db.SaveChanges();

             return vehiculo;

       }


        public IEnumerable<object> getVehiculobyPlaca(string _placa){
            
            var vehiculo = _db.vehiculos.Where(
                v => v.placa == _placa
            ).Select(x=> new {tipoVehiculo=x.Tipo_Vehiculo.tipo, vehiculo= x } ).ToList();

            return vehiculo;
       }


          public IEnumerable<object> getVehiculobyidentificacion(string _identificacion){
            
            var vehiculos =
            from v in _db.vehiculos
            join e in _db.empleados on v.id_empleado equals e.id_empleado
            where e.identificacion == _identificacion
            select new { vehiculo=v , tipoVehiculo= v.Tipo_Vehiculo.tipo};
            
            /*  var vehiculo = _db.vehiculos.Where(
                v => v.placa == _identificacion
            ).Select(x=> new {tipoVehiculo=x.Tipo_Vehiculo.tipo, vehiculo= x } ).ToList();
            */        

            return vehiculos;
       }


    }

}