using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ParkingApp.Models
{
    public class tipo_vehiculo{
        
         [Key]
         public int id_tipovehiculo { get; set; }
        public string tipo { get; set; }

        public virtual ICollection<vehiculos> vehiculos { get; set; }
    }
}