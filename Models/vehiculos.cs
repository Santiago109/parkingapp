using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ParkingApp.Models
{
    public class vehiculos{

        internal string _atributos { get; set; }

        [Key]
        public int id_vehiculo { get; set; }
        public string placa { get; set; }
        
        [NotMapped]
        public atributos atributos
        {
                get { return _atributos == null ? null : JsonConvert.DeserializeObject<atributos>(_atributos); }
                set { _atributos = JsonConvert.SerializeObject(value); }
        }
        public string foto { get; set; }
        public int id_tipovehiculo { get; set; }
        public int id_empleado { get; set; }
        public virtual tipo_vehiculo Tipo_Vehiculo { get; set; }

    }


    public class atributos{
        public string cilindraje { get; set; }
        public string tiempos { get; set; }
        public string modelo { get; set; }
        public string numpuertas { get; set; }
        public string color { get; set; }
        public string marca { get; set; }
        
    }


 
}