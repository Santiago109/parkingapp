using System;
using System.ComponentModel.DataAnnotations;

namespace ParkingApp.Models
{
    public class registro_ingresos{
        [Key]
        public int id_registro_ingreso { get; set; }
        public int id_vehiculo { get; set; }
        public DateTime fecha { get; set; }
        public string tipo { get; set; }
        public string celda { get; set; }
        
    }
}