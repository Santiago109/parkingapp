using System.ComponentModel.DataAnnotations;

namespace ParkingApp.Models
{
    public class  empleados{
        [Key]
        public int id_empleado { get; set; }
        public string identificacion { get; set; }
        public string nombre { get; set; }
        public string password { get; set; }
    }
}