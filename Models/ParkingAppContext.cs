using Microsoft.EntityFrameworkCore;


namespace ParkingApp.Models
{

    public class ParkingAppContext: DbContext
    {

        public ParkingAppContext(DbContextOptions<ParkingAppContext> options): base(options){
            
        } 

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           
            modelBuilder.Entity<vehiculos>()
                .Property(b => b._atributos).HasColumnName("atributos");
        }

        public DbSet<empleados> empleados {get;set;}  
        public DbSet<vehiculos> vehiculos {get;set;} 
        public DbSet<tipo_vehiculo> tipo_vehiculo {get;set;} 
        public DbSet<registro_ingresos> registro_ingresos {get;set;} 

       
 
    }


}