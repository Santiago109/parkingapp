﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ParkingApp.Models;
using ParkingApp.DAO;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;

namespace ParkingApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpleadosController : ControllerBase
    {
         DAOEmpleados daoEmpleados ; 
        public EmpleadosController(ParkingAppContext context){
             daoEmpleados = new DAOEmpleados(context);
         }

        //GET api/empleados
        [HttpGet]
        public IEnumerable<empleados> Get()
        {
            try
            {
                 return daoEmpleados.getEmpleados();
            }
            catch (System.Exception )
            {                
                 return null;
            }          
        }

        // GET api/empleados/5
        [HttpGet("{id}")]
        public ActionResult<empleados> Get(int id)
        {
            try
            {
                 var Empleado = daoEmpleados.getEmpleadoById(id);
                 return Empleado;
            }
            catch (System.Exception)
            {                
                 return BadRequest();
            }

        }

        // POST api/empleados/insert
        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPost("insert")]
        public void Post([FromBody] empleados empleado)
        {
           daoEmpleados.insertEmpleado(empleado);
        }

        // PUT api/empleados/update/5
        [HttpPut("update")]
        public void Put( [FromBody]  empleados empleado)
        {
            daoEmpleados.updateEmpleado(empleado);             
        }

        // DELETE api/values/5
        [HttpDelete("delete/{id}")]
        public void Delete(int id)
        {
            daoEmpleados.deleteEmpleado(id);          
        }


   
        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPost("login")]
        public IEnumerable<empleados> Login([FromBody] empleados empleado)
        {  
           return daoEmpleados.empleadoLogin(empleado.identificacion,empleado.password );
        }


       
        [Consumes("application/json")]
        [HttpPost("register")]
        public ActionResult RegistroEmpleado([FromBody] empleados empleado)
        {
            try
            { 
                 var empleadoNuevo =     daoEmpleados.registraEmpleado(empleado);
                return Ok(empleadoNuevo);

            }
            catch (System.Exception ex)
            {
               return ValidationProblem();
            
            }           
        }


  [HttpGet("reporte/{year}/{month}")]
        public IEnumerable<object> Get(int year,int month )
        {
            try
            {
                 var reporte = daoEmpleados.getReporte(year,month);
                 return reporte;
            }
            catch (System.Exception)
            {                
                 return null;
            }

        }
        

    }
}
