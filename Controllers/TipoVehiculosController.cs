﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ParkingApp.Models;
using ParkingApp.DAO;

namespace ParkingApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoVehiculosController : ControllerBase
    {
         DAOtipovehiculo daoTipoVehiculos ; 


        public TipoVehiculosController(ParkingAppContext context){
             daoTipoVehiculos = new DAOtipovehiculo(context);

         }

  
        [HttpGet]
        public IEnumerable<tipo_vehiculo> Get()
        {
            try
            {
                 return daoTipoVehiculos.getTipoVehiculo();
            }
            catch (System.Exception )
            {
                
                 return null;
            }
          
        }

        
        [HttpGet("{id}")]
        public ActionResult<tipo_vehiculo> Get(int id)
        {
                try
            {
                 var TipoVehiculo = daoTipoVehiculos.getTipoVehiculoById(id);
                 return TipoVehiculo;
            }
            catch (System.Exception)
            {
                
                 return BadRequest();
            }

        }

 
        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPost("insert")]
        public void Post([FromBody] tipo_vehiculo Tipovehiculo)
        {
           daoTipoVehiculos.insertTipoVehiculo(Tipovehiculo);           
        }

       
        [HttpPut("update")]
        public void Put(  [FromBody]  tipo_vehiculo Tipovehiculo)
        {
              daoTipoVehiculos.updateTipoVehiculo(Tipovehiculo);             
        }

       
        [HttpDelete("delete/{id}")]
        public void Delete(int id)
        {

            daoTipoVehiculos.deleteTipoVehiculo(id);
           
          
        }
    }
}
