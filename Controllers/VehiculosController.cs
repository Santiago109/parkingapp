﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ParkingApp.Models;
using ParkingApp.DAO;

namespace ParkingApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiculosController : ControllerBase
    {
         DAOVehiculos daoVehiculos ; 
 
        public VehiculosController(ParkingAppContext context){
             daoVehiculos = new DAOVehiculos(context);
        }
  
        [HttpGet]
        public IEnumerable<vehiculos> Get()
        {
            try
            {
                 return daoVehiculos.getVehiculos();
            }
            catch (System.Exception )
            {
                
                 return null;
            }          
        }
        
        [HttpGet("{id}")]
        public ActionResult<vehiculos> Get(int id)
        {
            try
            {
                 var vehiculo = daoVehiculos.getVehiculoById(id);
                 return vehiculo;
            }
            catch (System.Exception)
            {
                
                 return BadRequest();
            }
        }

          [HttpGet("empleado/{id}")]
        public IEnumerable<object> GetByEmpleado(int id)
        {
                try
            {
                 var vehiculos = daoVehiculos.getVehiculoByEmpleado(id);
                 return vehiculos;
            }
            catch (System.Exception)
            {
                
                 return null;
            }

        }

 
        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPost("insert")]
        public void Post([FromBody] vehiculos vehiculo)
        {
           daoVehiculos.insertVehiculo(vehiculo);           
        }
       
        [HttpPut("update")]
        public void Put( [FromBody]  vehiculos vehiculo)
        {
            daoVehiculos.updateVehiculo(vehiculo);             
        }

       
        [HttpDelete("delete/{id}")]
        public void Delete(int id)
        {
            daoVehiculos.deleteVehiculo(id);  
        }


       
        [Consumes("application/json")]
        [HttpPost("register")]
        public ActionResult RegistroVehiculo([FromBody] vehiculos vehiculo)
        {
            try
            { 
                var vehiculonuevo = daoVehiculos.registraVehiculo(vehiculo);
                return Ok(vehiculonuevo);
            }
            catch (System.Exception ex)
            {
               return ValidationProblem(); 
            }        
        }


        [HttpGet("Buscar/{placa}")]
        public IEnumerable<object>  BuscarVehiculoxPlaca(string placa)
        {
            try
            {
                 var vehiculo = daoVehiculos.getVehiculobyPlaca(placa);
                 return vehiculo;
            }
            catch (System.Exception)
            {
                
                 return null;
            }
        }


         [HttpGet("BuscarI/{identificacion}")]
        public IEnumerable<object>  BuscarVehiculosXporIdentificacion(string identificacion)
        {
            try
            {
                 var vehiculo = daoVehiculos.getVehiculobyidentificacion(identificacion);
                 return vehiculo;
            }
            catch (System.Exception)
            {
                
                 return null;
            }
        }
    }
}
