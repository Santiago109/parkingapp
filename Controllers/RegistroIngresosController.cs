using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ParkingApp.Models;
using ParkingApp.DAO;

namespace ParkingApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistroIngresosController : ControllerBase
    {
         private  ParkingAppContext _db;

        public RegistroIngresosController( ParkingAppContext context){
            _db =  context;
        }

  [HttpGet]
        public IEnumerable<registro_ingresos> Get()
        {
            try
            {
                 return _db.registro_ingresos.ToList();
            }
            catch (System.Exception )
            {                
                 return null;
            }          
        }

        // GET api/empleados/5
        [HttpGet("{id}")]
        public ActionResult<registro_ingresos> Get(int id)
        {
            try
            {
                 var registros = _db.registro_ingresos.Find(id);
                 return registros;
            }
            catch (System.Exception)
            {
                
                 return BadRequest();
            }

        }
        
        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPost("insert")]
        public void Post([FromBody] registro_ingresos registroIngreso)
        {
           _db.registro_ingresos.Add(registroIngreso);   
           _db.SaveChanges();        
        }
      
    }
}